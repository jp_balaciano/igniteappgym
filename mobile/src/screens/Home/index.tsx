import { Text, View, FlatList, ScrollView, TouchableOpacity} from "react-native";
import { styles } from "./styles";
import { useState } from "react";
import { useNavigation } from "@react-navigation/native";

import { AppNavigatorRoutesProps } from "../../routes/app.routes";

import HomeHeader from "../../components/HomeHeader";
import Card_exercicio from "../../components/Card_exercicio";
import Grupo from "../../components/Grupo";


export default function Home(){
    const [exercicios, setexercicios] = useState(['Remada curvada', 'Puxada frontal', 'Remada unilateral', 'Levantamento Terra'])

    const navigation = useNavigation<AppNavigatorRoutesProps>();

    function handleOpenExercicio() {
        navigation.navigate('Exercise');
    }

    return(
        <ScrollView showsVerticalScrollIndicator={false}>
            <View style={styles.container}>
                <HomeHeader />

                <ScrollView horizontal showsHorizontalScrollIndicator={false} style={styles.grupos}>
                    <Grupo 
                        name='costas'
                    />
                    <Grupo 
                        name='Ombro'
                    />                    
                    <Grupo 
                        name='Bíceps'
                    />                    
                    <Grupo 
                        name='Tríceps'
                    />
                </ScrollView>
                <View style={styles.texto}>
                    <Text style={styles.h1}>Exercícios</Text>
                    <Text style={styles.h2}>{exercicios.length}</Text>
                </View>

                <FlatList 
                    showsVerticalScrollIndicator={false}
                    data={exercicios}
                    keyExtractor={item => item}
                    renderItem={({item}) => (
                        <TouchableOpacity onPress={handleOpenExercicio}>
                            <Card_exercicio />
                        </TouchableOpacity>
                    )}
                    contentContainerStyle={styles.exercicios}
                />
            </View>
            </ScrollView>
        
    )
}