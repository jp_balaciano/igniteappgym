import { StyleSheet } from "react-native";


export const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: "#121214",
    },

    grupos: {
        marginTop: 20,
        marginBottom: 30,
        marginLeft: 15        
    },


    texto: {
        flexDirection: "row",
        alignItems: "center",
        justifyContent: "space-between",
        padding: 20
    },

    h1: {
        fontSize: 16,
        fontWeight: "bold",
        color: "#C4C4CC"
    },

    h2: {
        fontSize: 14,
        fontWeight: "400",
        color: "#C4C4CC"
    },

    exercicios: {
        flexDirection: "column",
        gap: 15,
        alignItems: "center",
        paddingBottom: 100
    }


});