import { Text, View, TouchableOpacity, Image, ScrollView, TextInput} from "react-native";
import { styles } from "./styles";

import { useState } from "react";
import * as ImagePicker from "expo-image-picker";

import ScreenHeader from "../../components/ScreenHeader";
import Button_acesso from "../../components/Button_acesso";


export default function Profile(){
    const [fotoEscolhida, setFotoEscolhida] = useState(false);
    const [userPhoto, setUserPhoto] = useState('https://secure.gravatar.com/avatar/fc93f404c7c004b12252a3ff18d521b03bea37cf886f3e826d4e7fbdc9e69329?s=1600&d=identicon');

    async function handleUserPhotoSelect() {
        setFotoEscolhida(true);

        try {
            const photoSelected = await ImagePicker.launchImageLibraryAsync({
                mediaTypes: ImagePicker.MediaTypeOptions.Images, //Definir o tipo de conteúdo que queremos selecionar da galeria do usuário (Imagens, vídeos e etc)
                quality: 1, //Definir a qualidade da imagem
                aspect: [4, 4], //Definir se ela é 4x4 e etc
                allowsEditing: true //Permitir que o usuário possa editar ela quando selecionar
            });
    
            if (photoSelected.canceled){ //Se o usuário cancelou nem precisa continuar
                return;
            }

            if (photoSelected.assets[0].uri) {
                setUserPhoto(photoSelected.assets[0].uri); //selecionar a imagem e pegar da posição 0
            }
    
        } catch (error) {
            console.log(error);
        } finally {
            setFotoEscolhida(false);
        }
    }


    return(
        <ScrollView contentContainerStyle={{flexGrow: 1}} showsVerticalScrollIndicator={false}>
            <View style={styles.container}>
                <ScreenHeader 
                    title="Perfil"
                />

                <View style={styles.foto_perfil}>
                    {
                        fotoEscolhida ? 
                        <Image source={require("../../assets/profile.png")} style={styles.foto}/>
                        :
                        <Image source={{uri: userPhoto}} style={styles.foto}/>
                    }
                    <TouchableOpacity onPress={handleUserPhotoSelect}>
                        <Text style={styles.alterar}>Alterar foto</Text>
                    </TouchableOpacity>
                </View>

                <View style={styles.form}>
                    <TextInput
                        style={styles.input} 
                        placeholder="Nome completo"
                        placeholderTextColor="#757576"
                    />
                    <TextInput
                        style={styles.input} 
                        value="joaoporta@id.uff.br"
                        placeholderTextColor="#C4C4CC"
                        editable={false}
                    />

                </View>

                <View style={styles.form}>
                    <Text style={styles.h2}>Alterar senha</Text>
                    <TextInput 
                        style={styles.input}
                        placeholder="Senha antiga"
                        placeholderTextColor="#757576"
                        secureTextEntry
                    />
                    <TextInput 
                        style={styles.input}
                        placeholder="Nova senha"
                        placeholderTextColor="#757576"
                        secureTextEntry
                    />
                    <TextInput 
                        style={styles.input}
                        placeholder="Confirmar nova senha"
                        placeholderTextColor="#757576"
                        secureTextEntry
                    />
                </View>

                <Button_acesso 
                    title="Atualizar"
                />
            </View>
        </ScrollView>
        
    )
}