import { StyleSheet } from "react-native";


export const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: "#121214",
        alignItems: "center",
        paddingBottom: 60
    },

    foto_perfil: {
        alignItems: "center",
        gap: 15,
        marginTop: 24
    },

    foto: {
        width: 140,
        height: 148,
        borderRadius: 1000,
        borderWidth: 2,
        borderColor: "#323238"
    },

    alterar: {
        color: "#00b37e",
        fontSize: 16,
        fontWeight: "bold"
    },

    form: {
        gap: 15,
        marginBottom: 30,
        marginTop: 20
    },

    input: {
        width: 348,
        height: 56,
        padding: 15,
        backgroundColor: "#202024",
        borderRadius: 6,
        color: "#FFFFFF",
        fontSize: 16,
        fontWeight: "400"
    },

    h2: {
        color: "#C4C4CC",
        fontSize: 16,
        fontWeight: "bold"
    },
});