import { StyleSheet } from "react-native";


export const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: "#121214",
    },

    content: {
        alignItems: "center",
        marginTop: 30,
        gap: 20,
        paddingBottom: 60
    },

    details: {
        backgroundColor: "#202024",
        width: 364,
        height: 145,
        borderRadius: 8,
        justifyContent: "center",
        alignItems: "center",
        gap: 20
    },

    info: {
        flexDirection: "row",
        gap: 30
    },

    series: {
        flexDirection: "row",
        gap: 10
    },

    halter: {
        width: 24,
        height: 24,
    },

    h2: {
        color: "#c4c4cc",
        fontWeight: "400",
        fontSize: 18
    },

 
});