import { Text, View, TouchableOpacity, Image, ImageBackground, ScrollView} from "react-native";
import { styles } from "./styles";
import ExHeader from "../../components/ExHeader";
import Button_acesso from "../../components/Button_acesso";



export default function Exercise(){

    return(
        <View style={styles.container}>
            <ExHeader />

            <ScrollView contentContainerStyle={{flexGrow: 1}} showsVerticalScrollIndicator={false}>
                <View style={styles.content}>
                        <Image source={require("../../assets/exexecucao.png")}/>
                        <View style={styles.details}>
                            <View style={styles.info}>
                                <View style={styles.series}>
                                    <Image source={require("../../assets/logo.png")} style={styles.halter}/>
                                    <Text style={styles.h2}>3 séries</Text>
                                </View>
                                <View style={styles.series}>
                                    <Image source={require("../../assets/repeticao.png")}/>
                                    <Text style={styles.h2}>12 repetições</Text>
                                </View>
                            </View>

                            <TouchableOpacity>
                                <Button_acesso 
                                    title="Marcar como realizado"
                                />
                            </TouchableOpacity>
                        </View>
                    </View>
                </ScrollView>
            </View>
    )
}

