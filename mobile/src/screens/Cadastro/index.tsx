import { Text, View, TextInput, Image, ImageBackground, ScrollView, TouchableOpacity, Alert} from "react-native";
import { styles } from "./styles";

import { useForm, Controller } from "react-hook-form"; //
import { useNavigation } from "@react-navigation/native";

//import * as yup from "yup"; //
//import yupResolver from "@hookform/resolvers"
import { api } from "../../services/api";
import { AppError } from "../../utils/AppErro";


import Titulo from "../../components/Titulo";
import Button_acesso from "../../components/Button_acesso";
import Button_final from "../../components/Butoon_final";
//import { date } from "yup";
// import { ValidationError } from "yup";

type FormDataProps = {  //Tipagem dos dados 
    name: string;
    email: string;
    senha: string;
    senhaConfirmada: string;
}

//Instalar o schema validation
//npm install @hookform/resolvers yup

//const cadastroSchema = yup.object({
   // name: yup.string().required("Informe o seu nome."),
    //email: yup.string().required("Informe o seu e-mail").email("E-mail inválido"),
    //senha: yup.string().required("Informe a sua senha").min(6, "A senha deve ter pelo menos 6 dígitos"),
    //senhaConfirmada: yup.string().required("Confirme a sua senha").oneOf([yup.ref("senha"), null], "As senhas estão diferentes")
//});


export default function Cadastro(){
    const { control, handleSubmit, formState: { errors } } = useForm<FormDataProps>({
        //resolver: yupResolver(cadastroSchema)
    }); //


    const navigation = useNavigation();

    function handleGoBack(){
        navigation.goBack();
    }


    async function handleCadastro({name, email, senha, senhaConfirmada}: FormDataProps) { 
        try{
            const response = await api.post("/users", {name, email, senha});
        }catch(error){
            const isAppError = error instanceof AppError;
            const title = isAppError ? error.message : "Não foi possível criar a conta. Tente novamente mais tarde"
            
            Alert.alert(title);
        }
    }

    return(
        <ScrollView contentContainerStyle={{flexGrow: 1}} showsVerticalScrollIndicator={false}>
            <View style={styles.container}>
                <ImageBackground
                defaultSource={require("../../assets/background.png")}
                style={styles.imagemFundo}
                alt="Pessoas Treinando"
                >
                    <Titulo />
                    <View style={styles.content}>
                        <Text style={styles.h3}>Crie sua conta</Text>
                        <View style={styles.forms}>

                            <Controller //
                                control={control}
                                name="name"
                                //Se usar o yup é so tirar essas rules e usar o yup
                                rules={{  //Regras para a validação
                                    required: "Informe o nome.", //Obrigatório
                                }}  
                                render={({field: {onChange, value}}) => (
                                    <View style={styles.erro}>
                                        <TextInput style={styles.input}
                                            placeholder="Nome"
                                            placeholderTextColor="#7c7c8a"
                                            onChangeText={onChange}
                                            value={value}
                                        />
                                        {errors.name && <Text style={styles.mensagemErro}>{errors.name?.message}</Text>}
                                    </View>
                                )}
                            />


                            <Controller 
                                control={control}
                                name="email"
                                rules={{
                                    required: "Informe o Email.",
                                    pattern: {
                                        value: /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i, //Validação do email
                                        message: "E-mail inválido" //Mensagem de erro
                                    }
                                }}
                                render={({field: {onChange, value}}) => (
                                    <View style={styles.erro}>
                                        <TextInput style={styles.input}
                                            placeholder="E-mail"
                                            placeholderTextColor="#7c7c8a"
                                            autoCapitalize="none"
                                            onChangeText={onChange}
                                            value={value}
                                        />
                                        {errors.email && <Text style={styles.mensagemErro}>{errors.email?.message}</Text>}
                                    </View>
                                )}
                            />

                            <Controller 
                                control={control}
                                name="senha"
                                rules={{
                                    required: "Escolha uma senha",
                                }}
                                render={({field: {onChange, value}}) => (
                                    <View style={styles.erro}>
                                        <TextInput style={styles.input}
                                            placeholder="Senha"
                                            placeholderTextColor="#7c7c8a"
                                            secureTextEntry
                                            onChangeText={onChange}
                                            value={value}
                                        />
                                        {errors.senha && <Text style={styles.mensagemErro}>{errors.senha?.message}</Text>}
                                    </View>
                                )}
                            />

                            <Controller 
                                control={control}
                                name="senhaConfirmada"
                                rules={{
                                    required: "Confirme a sua senha",
                                }}
                                render={({field: {onChange, value}}) => (
                                    <View style={styles.erro}>
                                        <TextInput style={styles.input}
                                            placeholder="Confirmar senha"
                                            placeholderTextColor="#7c7c8a"
                                            secureTextEntry
                                            onChangeText={onChange}
                                            value={value}
                                            onSubmitEditing={handleSubmit(handleCadastro)}
                                            returnKeyType="send"
                                        />

                                        {errors.senhaConfirmada && <Text style={styles.mensagemErro}>{errors.senhaConfirmada?.message}</Text>}
                                    </View>
                                )}
                            />
                        </View>

                        <TouchableOpacity onPress={handleSubmit(handleCadastro)}>
                            <Button_acesso 
                                title="Criar e acessar"
                            />
                        </TouchableOpacity>

                        <TouchableOpacity onPress={handleGoBack}>
                            <Button_final 
                                    title="Voltar para o login"
                            />
                        </TouchableOpacity>
                        
                    </View>
                </ImageBackground>
            </View>
        </ScrollView>

    )
}


