import { Text, View, TouchableOpacity, Image, SectionList, ScrollView} from "react-native";
import { styles } from "./styles";


import ScreenHeader from "../../components/ScreenHeader";
import CardHistorico from "../../components/CardHistorico";
import { useState } from "react";



export default function Historico(){

    const [exercicios, setexercicios] = useState([
        {
            title: "26.08.24",
            data: ["Puxada Frontal", "Remada unilateral"]
        },
        {
            title: "25.08.24",
            data: ["Puxada Frontal"]
        },
    ]);

    return(
        <ScrollView contentContainerStyle={{flexGrow: 1}} showsVerticalScrollIndicator={false}>
            <View style={styles.container}>
                <ScreenHeader 
                    title="Histórico de Exercícios"
                />

                <SectionList
                    sections={exercicios}
                    keyExtractor={item => item}
                    renderItem={({item}) => (
                        <CardHistorico />
                    )}
                    renderSectionHeader={({section}) => (
                        <Text style={styles.data}>{section.title}</Text>
                    )}
                    contentContainerStyle={exercicios.length === 0 && {flex: 1, justifyContent: "center"}}
                    ListEmptyComponent={() => (
                        <>
                        <Text style={styles.semCard}>Não há exercícios registrados ainda.</Text>
                        <Text style={styles.semCard}>Vamos fazer exercicios hoje?</Text>
                        </>

                    )}
                />

            </View>
        </ScrollView>
        
    )
}