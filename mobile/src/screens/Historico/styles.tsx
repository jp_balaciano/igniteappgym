import { StyleSheet } from "react-native";


export const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: "#121214",
    },

    cards: {
        alignItems: "center",
        gap: 15,
        marginTop: 60
    },

    data: {
        fontSize: 16,
        fontWeight: "bold",
        color: "#C4C4CC",
        marginTop: 40,
        marginBottom: 15,
        marginLeft: 20
    },

    semCard: {
        color: "#C4C4CC",
        textAlign: "center"
    },
});