import { Text, View, TouchableOpacity, TextInput, ImageBackground, ScrollView} from "react-native";
import { styles } from "./styles";

import { useNavigation } from "@react-navigation/native";
import { AuthNavigatorRoutesProps } from "../../routes/auth.routes";

import Titulo from "../../components/Titulo";
import Button_acesso from "../../components/Button_acesso";
import Button_final from "../../components/Butoon_final";

export default function Login(){

    const navigation = useNavigation<AuthNavigatorRoutesProps>();

    function handleNewAccount(){
        navigation.navigate("Cadastro");
    }

    return(
        <ScrollView contentContainerStyle={{flexGrow: 1}} showsVerticalScrollIndicator={false}>
            <View style={styles.container}>
                <ImageBackground
                defaultSource={require("../../assets/background.png")}
                style={styles.imagemFundo}
                alt="Pessoas Treinando"
                >
                    <Titulo />
                    <View style={styles.content}>
                        <Text style={styles.h3}>Acesse sua conta</Text>
                        <View style={styles.forms}> 
                            <TextInput style={styles.input}
                                placeholder="E-mail"
                                placeholderTextColor="#7c7c8a"
                            />
                            <TextInput style={styles.input}
                                placeholder="Senha"
                                placeholderTextColor="#7c7c8a"
                            />
                        </View>

                        <TouchableOpacity>
                            <Button_acesso 
                                title="Acessar"
                            />
                        </TouchableOpacity>

                        <View style={styles.conta}>
                            <Text style={styles.h4}>Ainda não tem acesso?</Text>
                            <TouchableOpacity onPress={handleNewAccount}>
                                <Button_final 
                                    title="Criar conta"
                                />
                            </TouchableOpacity>
                        </View>
                    </View>
                </ImageBackground>
            </View>
        </ScrollView>
        
    )
}