import { StyleSheet } from "react-native";


export const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: "#121214",
    },

    imagemFundo: {
        flex: 1,
        resizeMode: "contain",
        width: "100%",
        alignSelf: 'center',
    },

    content: {
        alignItems: "center",
        gap: 30,
        marginTop: 120
    },

    h3: {
        color: "#E1E1E6",
        fontSize: 20,
        fontWeight: "bold"
    },

    forms: {
        gap: 15
    },

    input: {
        width: 348,
        height: 56,
        padding: 15,
        backgroundColor: "#121214",
        borderRadius: 6,
        color: "#FFFFFF",
        fontSize: 16,
        fontWeight: "400"
    },

    conta: {
        flexDirection: "column",
        gap: 15,
        alignItems: "center",
        marginTop: 80
    },

    h4: {
        color: "#e1e1e6",
        fontSize: 16,
        fontWeight: "400"
    },

});