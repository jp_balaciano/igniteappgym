import { createNativeStackNavigator, NativeStackNavigationProp } from "@react-navigation/native-stack";

import Login from "../screens/Login";
import Cadastro from "../screens/Cadastro";

type AuthRoutes = {
    Login: undefined;  //Sem parametro passado entre a rota por isso undefined 
    Cadastro: undefined; 
}

//A que vamos exportar para ser reutilizada quando formos usar a tipagem
export type AuthNavigatorRoutesProps = NativeStackNavigationProp<AuthRoutes>;

const { Navigator, Screen } = createNativeStackNavigator<AuthRoutes>();

export default function AuthRoutes(){
    return (
        <Navigator screenOptions={{headerShown: false}}>
            <Screen 
                name="Login"
                component={Login}
            />

            <Screen 
                name="Cadastro"
                component={Cadastro}
            />
        </Navigator>
    )
}