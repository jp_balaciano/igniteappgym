import { createBottomTabNavigator, BottomTabNavigationProp } from "@react-navigation/bottom-tabs";
import { Image } from "react-native";


import Home from "../screens/Home";
import Exercise from "../screens/Exercise";
import Profile from "../screens/Profile";
import Historico from "../screens/Historico";


//Tipagem das rotas privadas
type AppRoutes = {
    Home: undefined;
    Exercise: undefined;
    Profile: undefined;
    Historico: undefined;
}

export type AppNavigatorRoutesProps = BottomTabNavigationProp<AppRoutes>;

const {Navigator, Screen} = createBottomTabNavigator<AppRoutes>();


export function AppRoutes(){

    return(
        <Navigator screenOptions={{
            headerShown: false,
            tabBarShowLabel: false, //Não mostra o texto
            tabBarActiveTintColor: "green",
            tabBarInactiveTintColor: "gray",
            tabBarStyle: {
                backgroundColor: "#202024",
                borderTopWidth: 0,

            }
        }}>
            <Screen 
                name="Home"
                component={Home}
                options={{
                    tabBarIcon: () => (
                        <Image source={require("../assets/home.png")}/>
                    ),
                }}
            />

            <Screen 
                name="Historico"
                component={Historico}
                options={{
                    tabBarIcon: () => (
                        <Image source={require("../assets/history.png")}/>

                    )
                }}
            />

            <Screen 
                name="Profile"
                component={Profile}
                options={{
                    tabBarIcon: () => (
                        <Image source={require("../assets/profile.png")} />
                    )
                }}
            />

            <Screen 
                name="Exercise"
                component={Exercise}
                options={{ tabBarButton: () => null}} //Faz com que fique na rota da aplicação, mas não apareça no tab bottom
            />

        </Navigator>
    );
}