import { Text, View, Pressable} from "react-native";
import { styles } from "./styles";
import { useState } from "react";

type Props = {
    name: string;
}

export default function Grupo({name}: Props){
    const [isPressed, setIsPressed] = useState(false);

    return(
        <View style={styles.container}>
            <Pressable
                onPressIn={() => setIsPressed(true)}
                onPressOut={() => setIsPressed(false)}
                style={({pressed}) => [
                    {
                        borderColor: pressed ? "green" : isPressed ? "green" : null,
                        borderWidth: 1,
                    },
                    styles.container
                ]}
            >
                <Text style={styles.h1}>{name}</Text>
            </Pressable>
        </View>
        
    )
}