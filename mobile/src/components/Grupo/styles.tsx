import { StyleSheet } from "react-native";



export const styles = StyleSheet.create({
    container: {
        width: 96,
        height: 50,
        backgroundColor: "#202024",
        borderRadius: 4,
        alignItems: "center",
        justifyContent: "center",
        overflow: "hidden",
        marginRight: 10,
    },

    h1: {
        fontSize: 12,
        fontWeight: "bold",
        color: "#C4C4CC",
        textTransform: "uppercase",
    }

});