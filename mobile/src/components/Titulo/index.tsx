import { Text, View, Image} from "react-native";
import { styles } from "./styles";


export default function Titulo(){
    return(
        <View style={styles.container}>
            <View style={styles.logo}>
                <Image source={require("../../assets/logo.png")}/>
                <Text style={styles.h1}>Ignite Gym</Text>
            </View>
            <Text style={styles.h2}>Treine sua mente e seu corpo</Text>
        </View>
        
    )
}