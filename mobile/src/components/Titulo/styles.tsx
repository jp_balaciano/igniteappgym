import { StyleSheet } from "react-native";


export const styles = StyleSheet.create({
    container: {
        flexDirection: 'column',
        alignItems: 'center',
        marginTop: 130

    },

    logo: {
        flexDirection: 'row',
        gap: 15
    },

    h1: {
        fontSize: 28,
        fontWeight: "bold",
        color: "#FFFFFF"
    },

    h2: {
        fontSize: 14,
        fontWeight: "400",
        color: "#FFFFFF"
    }

});