import { StyleSheet } from "react-native";


export const styles = StyleSheet.create({
    container: {
        backgroundColor: "#29292e",
        width: 364,
        height: "auto",
        minHeight: 83,
        justifyContent: "space-between",
        padding: 15,
        alignItems: "center",
        flexDirection: "row",

    },

    content: {
        flexDirection: "row",
        gap: 15,
        alignItems: "center"
    },

    imagem: {
        width: 70,
        height: 70,
        borderRadius: 6,
        resizeMode: "cover"
    },

    textos: {
        flexDirection: "column",
        gap: 10,
        width: "70%"
    },

    h1: {
        fontSize: 18,
        fontWeight: "bold",
        color: "#FFFFFF"
    },

    h2: {
        fontSize: 14,
        fontWeight: "400",
        color: "#C4C4CC",
    }
});