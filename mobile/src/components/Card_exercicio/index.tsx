import { Text, View, Image, TouchableOpacityProps} from "react-native";
import { styles } from "./styles";

type Props = TouchableOpacityProps & {

}

export default function Card_exercicio({...rest}: Props){
    return(
        <View style={styles.container}>
            <View style={styles.content}>
                <Image source={{uri: 'https://files.passeidireto.com/7dceacb9-8013-4e9c-b30b-dd174f2e525d/7dceacb9-8013-4e9c-b30b-dd174f2e525d.jpeg'}} alt="imagem do exercício" style={styles.imagem}/>
                <View style={styles.textos}>
                    <Text style={styles.h1}>Nome do Exercício</Text>
                    <Text style={styles.h2} numberOfLines={2}>Séries x Repetições</Text>
                </View>
            </View>
            <Image source={require("../../assets/entrar.png")}/>
        </View>
        
    )
}