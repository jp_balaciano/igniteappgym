import { StyleSheet } from "react-native";


export const styles = StyleSheet.create({
    container: {
       width: 364,
       height: 87,
       backgroundColor: "#202024",
       flexDirection: "row",
       justifyContent: "space-between",
       padding: 30,
       alignItems: "center",
       alignSelf: "center",
       marginBottom: 15
    },

    textos: {
        gap: 10,
        flex: 1
    },

   h1: {
    fontSize: 16,
    fontWeight: "bold",
    color: "#FFFFFF",
   },

   h2: {
    fontSize: 18,
    fontWeight: "400",
    color: "#e1e1e6"
   },

   hora: {
    fontSize: 16,
    fontWeight: "400",
    color: "#7C7C84"
   }

});