import { Text, View, Image, TouchableOpacity} from "react-native";
import { styles } from "./styles";

import { useNavigation } from "@react-navigation/native";

export default function ExHeader(){
    const navigation = useNavigation();

    function handleGoBack() {
        navigation.goBack();
    }

    return(
        <View style={styles.container}>
            <TouchableOpacity onPress={handleGoBack} style={styles.voltar}>
                <Image source={require("../../assets/voltar.png")}/>
            </TouchableOpacity>
            <View style={styles.textos}> 
                <Text style={styles.h1}>Puxada Frontal</Text>
                <View style={styles.grupo}>
                    <Image source={require("../../assets/exercicio.png")}/>
                    <Text style={styles.h2}>Costas</Text>
                </View>
            </View>
        </View>
        
    )
}