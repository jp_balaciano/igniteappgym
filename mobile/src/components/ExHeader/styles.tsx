import { StyleSheet } from "react-native";


export const styles = StyleSheet.create({
    container: {
        backgroundColor: "#202024",
        width: "100%",
        height: "auto",
        minHeight: 148, 
        flexDirection: "column",
        gap: 15
    },

    voltar: {
        marginTop: 60,
        marginLeft: 40
    },

    textos: {
        flexDirection: "row",
        alignItems: "center",
        justifyContent: "space-between",
        paddingLeft: 40,
        paddingRight: 40,
        marginBottom: 10
    },


    h1: {
        fontSize: 20,
        fontWeight: "bold",
        color: "#E1E1E6",
        flexShrink: 1
    },

    grupo: {
        flexDirection: "row",
        gap: 5
    },

    h2: {
        fontSize: 16,
        fontWeight: "400",
        color: "#c4C4CC"
    },

    
});