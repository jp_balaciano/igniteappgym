import { StyleSheet } from "react-native";


export const styles = StyleSheet.create({
    container: {
        width: 348,
        height: 56,
        borderRadius: 6,
        backgroundColor: "#00875F",
        alignItems: 'center',
        justifyContent: 'center'
    },

    texto: {
        color: "#FFFFFF",
        fontSize: 16,
        fontWeight: "bold",
    }

});