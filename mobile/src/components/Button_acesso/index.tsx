import { TouchableOpacity, Text, View } from "react-native";
import { styles } from "./styles";

type Props = {
    title: string;
}

export default function Button_acesso({title}: Props){

    return(
        <View style={styles.container}>
            <Text style={styles.texto}>{title}</Text>
        </View>

    )
}