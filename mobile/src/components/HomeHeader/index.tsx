import { Text, View, Image, TouchableOpacity} from "react-native";
import { styles } from "./styles";


export default function HomeHeader(){
    return(
        <View style={styles.container}>
            <View style={styles.content}>
                <Image source={require("../../assets/foto-linkedin.jpeg")} style={styles.imagem} alt="Imagem do usuário"/>
                <View>
                    <Text style={styles.h1}>Olá,</Text>
                    <Text style={styles.h2}>Nome do usuário</Text>
                </View>
            </View>
            <TouchableOpacity>
                <Image source={require("../../assets/Logout.png")} style={styles.sair}/>
            </TouchableOpacity>
        </View>
        
    )
}