import { StyleSheet } from "react-native";


export const styles = StyleSheet.create({
    container: {
        backgroundColor: "#202024",
        width: "100%",
        height: 148, 
        flexDirection: "row",
        alignItems: 'center',
        justifyContent: "space-around"
    },

    content: {
        flexDirection: "row",
        alignItems: "center",
        gap: 15,
        marginTop: 70
    },

    imagem: {
        width: 64,
        height: 64,
        borderRadius: 1000
    },

    h1: {
        fontSize: 16,
        fontWeight: '400',
        color: "#E1E1E6"
    },

    h2: {
        fontSize: 16,
        fontWeight: "bold",
        color: "#E1E1E6"
    },
    
    sair: {
        marginTop: 70
    }

});