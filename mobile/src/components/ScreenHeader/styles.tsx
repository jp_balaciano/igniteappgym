import { StyleSheet } from "react-native";


export const styles = StyleSheet.create({
    container: {
        backgroundColor: "#202024",
        width: "100%",
        height: 148, 
        alignItems: "center",
        justifyContent: "center",
    },

    h1: {
        fontSize: 20,
        fontWeight: "bold",
        color: "#E1e1e6",
        marginTop: 50
    },

});