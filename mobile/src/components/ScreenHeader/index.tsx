import { Text, View, Image, TouchableOpacity} from "react-native";
import { styles } from "./styles";

type Props = {
    title: string;
}


export default function ScreenHeader({title}: Props){
    return(
        <View style={styles.container}>
            <Text style={styles.h1}>{title}</Text>
        </View>
        
    )
}