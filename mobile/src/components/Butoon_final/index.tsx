import { View, Text } from "react-native";
import { styles } from "./styles";


type Props = {
    title: string;
}

export default function Button_final({title}: Props){

    return(
        <View style={styles.container}>
            <Text style={styles.texto}>{title}</Text>
        </View>

    )
}