import { StyleSheet } from "react-native";


export const styles = StyleSheet.create({
    container: {
        width: 348,
        height: 56,
        borderRadius: 6,
        backgroundColor: "#121214",
        borderWidth: 1,
        borderColor: "#00B37e",
        alignItems: 'center',
        justifyContent: 'center'
    },

    texto: {
        color: "#00b37e",
        fontSize: 16,
        fontWeight: "bold",
    }

});