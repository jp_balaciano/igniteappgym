import { StatusBar } from "react-native";
import { Routes } from "./src/routes";

import Historico from "./src/screens/Historico";

export default function App() {
  return (
    <>
      <StatusBar 
        barStyle="light-content"
        backgroundColor="transparent"
        translucent
      />

      <Routes />
    </>
  );
}

